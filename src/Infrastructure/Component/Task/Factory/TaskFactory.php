<?php

namespace Infrastructure\Component\Task\Factory;

use Domain\Task\Exception\TaskCreateException;
use Domain\Task\Factory\TaskFactoryInterface;
use Domain\Task\Entity\Task;
use Domain\Task\Entity\TaskCollection;
use Infrastructure\Component\Task\DTO\TaskAddDTO;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * Class TaskFactory
 * @package Infrastructure\Component\Task\Factory
 */
final class TaskFactory implements TaskFactoryInterface
{

    /**
     * @inheritdoc
     */
    public function create(TaskAddDTO $taskAddDTO): Task
    {
        try {
            $task = new Task(
                $taskAddDTO->name,
                $taskAddDTO->title
            );
        } catch (\Throwable $exception) {
            throw new TaskCreateException($exception);
        }

        return $task;
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data): TaskCollection
    {
        return new TaskCollection($data);
    }
}