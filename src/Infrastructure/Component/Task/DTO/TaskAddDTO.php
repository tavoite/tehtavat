<?php

namespace Infrastructure\Component\Task\DTO;

/**
 * Class TaskAddDTO
 * @package Infrastructure\Component\Task\DTO
 *
 * @property string $name
 * @property string $title
 */
class TaskAddDTO
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $title;

    /**
     * TaskAddDTO constructor.
     * @param string $name
     * @param string $title
     */
    public function __construct(string $name, string $title)
    {
        $this->name = $name;
        $this->title = $title;
    }
}