<?php

namespace Infrastructure\Component\Worker\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\Worker\Service\AbstractWorkersPoolStrategy;
use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;

/**
 * Class OmitLastTwoWorkersStrategy
 * @package Infrastructure\Component\Worker\Service
 */
final class OmitLastTwoWorkersStrategy extends AbstractWorkersPoolStrategy
{
    /**
     * @inheritdoc
     */
    public function createWorkersPool(
        WorkerCollection $workerCollection,
        WorkerTaskCollection $workerTaskCollection
    ): ArrayCollection
    {
        $workerTaskCollectionQuantity = $workerTaskCollection->count();
        $omitQuantity = $this->getOmitQuantity($workerTaskCollectionQuantity);

        $omitPool = array_map(function (WorkerTask $workerTask) {
            return $workerTask->worker()->id();
        }, array_slice($workerTaskCollection->getItems(), $workerTaskCollectionQuantity - $omitQuantity));

        $idsPool = array_map(function (Worker $worker) {
            return $worker->id();
        }, $workerCollection->getItems());

        $resultPool = array_diff($idsPool, $omitPool);

        return new ArrayCollection($resultPool);
    }

    /**
     * @param int $workerTaskCollectionQuantity
     * @return int
     */
    private function getOmitQuantity(int $workerTaskCollectionQuantity): int
    {
        if ($workerTaskCollectionQuantity <= 1) {
            $omitQuantity = 0;
        } else if ($workerTaskCollectionQuantity === 2) {
            $omitQuantity = 1;
        } else {
            $omitQuantity = 2;
        }

        return $omitQuantity;
    }
}