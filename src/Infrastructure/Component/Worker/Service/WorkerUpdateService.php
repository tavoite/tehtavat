<?php

namespace Infrastructure\Component\Worker\Service;

use Domain\Worker\Service\WorkerUpdateServiceInterface;
use Domain\Worker\Entity\Worker;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;

/**
 * Class WorkerUpdateService
 * @package Infrastructure\Component\Worker\Service
 */
final class WorkerUpdateService implements WorkerUpdateServiceInterface
{
    /**
     * @inheritdoc
     */
    public function update(Worker $worker, WorkerUpdateDTO $workerUpdateDTO): Worker
    {
        if (!empty($workerUpdateDTO->name)) {
            $worker->rename($workerUpdateDTO->name);
        }

        if (!empty($workerUpdateDTO->email)) {
            $worker->changeEmail($workerUpdateDTO->email);
        }

        return $worker;
    }
}