<?php

namespace Infrastructure\Component\Worker\Service;

use Domain\Task\Entity\Task;
use Domain\Worker\Exception\NoWorkersFoundException;
use Domain\Worker\Service\AbstractWorkersPoolStrategy;
use Domain\Worker\Service\AbstractRandomWorkerProviderService;
use Domain\Worker\Entity\Worker;

/**
 * Class RandomWorkerProviderService
 * @package Infrastructure\Component\Worker\Service
 */
final class RandomWorkerProviderService extends AbstractRandomWorkerProviderService
{
    /**
     * @inheritdoc
     */
    public function getRandomWorkerForTask(Task $task, AbstractWorkersPoolStrategy $workersPoolStrategy): Worker
    {
        $workerCollection = $this->workerRepository->getAll();

        if ($workerCollection->count() === 0) {
            throw new NoWorkersFoundException('Worker collection is empty');
        }

        $workerTaskCollection = $this->workerTaskRepository->getLastFewForTask($task, $workerCollection->count());

        $workersPool = $workersPoolStrategy->createWorkersPool($workerCollection, $workerTaskCollection);

        $randomWorkerId = $workersPool[array_rand($workersPool->toArray())];

        return $this->workerRepository->getOne($randomWorkerId);
    }
}