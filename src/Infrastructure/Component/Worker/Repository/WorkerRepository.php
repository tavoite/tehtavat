<?php

namespace Infrastructure\Component\Worker\Repository;

use Doctrine\ORM\EntityManager;
use Domain\Worker\Exception\NoWorkerFoundException;
use Domain\Worker\Exception\WorkerAddException;
use Domain\Worker\Exception\WorkerCreateException;
use Domain\Worker\Exception\WorkerUpdateException;
use Domain\Worker\Exception\WorkerValidationException;
use Domain\Worker\Factory\WorkerFactoryInterface;
use Domain\Worker\Service\WorkerUpdateServiceInterface;
use Domain\Worker\Entity\WorkerCollection;
use Domain\Worker\Repository\WorkerRepositoryInterface;
use Domain\Worker\Entity\Worker;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class WorkerRepository
 * @package Infrastructure\Component\Worker\Repository
 */
final class WorkerRepository implements WorkerRepositoryInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var WorkerFactoryInterface
     */
    private $workerFactory;

    /**
     * @var WorkerUpdateServiceInterface
     */
    private $workerUpdateService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * WorkerRepository constructor.
     * @param EntityManager $entityManager
     * @param WorkerFactoryInterface $workerFactory
     * @param WorkerUpdateServiceInterface $workerUpdateService
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManager $entityManager,
        WorkerFactoryInterface $workerFactory,
        WorkerUpdateServiceInterface $workerUpdateService,
        ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->workerFactory = $workerFactory;
        $this->workerUpdateService = $workerUpdateService;
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function add(WorkerAddDTO $workerAddDTO): Worker
    {
        try {
            $worker = $this->workerFactory->create($workerAddDTO);
        } catch (WorkerCreateException $exception) {
            throw new WorkerAddException('Problem occurred during new worker creation.');
        }

        $errors = $this->validator->validate($worker);

        if ($errors->count()) {
            throw new WorkerValidationException((string)$errors);
        }

        try {
            $this->entityManager->persist($worker);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new WorkerAddException('Problem occurred during saving new worker.');
        }

        return $worker;
    }

    /**
     * @inheritdoc
     */
    public function update(int $id, WorkerUpdateDTO $workerUpdateDTO): Worker
    {
        $worker = $this->getOne($id);

        if (is_null($worker)) {
            throw new NoWorkerFoundException('Worker does not exists.');
        }

        $worker = $this->workerUpdateService->update($worker, $workerUpdateDTO);
        $errors = $this->validator->validate($worker);

        if ($errors->count()) {
            throw new WorkerValidationException((string)$errors);
        }

        try {
            $this->entityManager->persist($worker);
            $this->entityManager->flush();
        } catch (\Exception $exception) {
            throw new WorkerUpdateException('Problem occurred during saving existing worker.');
        }

        return $worker;
    }

    /**
     * @inheritdoc
     */
    public function getOne(int $id): ?Worker
    {
        return $this->entityManager
            ->createQueryBuilder()
            ->select('w')
            ->from(Worker::class, 'w')
            ->where('w.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @inheritdoc
     */
    public function getAll(): WorkerCollection
    {
        $workers = $this->entityManager
            ->createQueryBuilder()
            ->select('w')
            ->from(Worker::class, 'w')
            ->getQuery()
            ->getResult();

        return $this->workerFactory->createCollection($workers);
    }
}