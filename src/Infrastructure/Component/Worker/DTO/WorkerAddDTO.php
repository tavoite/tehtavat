<?php

namespace Infrastructure\Component\Worker\DTO;

/**
 * Class WorkerAddDTO
 * @package Infrastructure\Component\Worker\DTO
 *
 * @property string $name
 * @property string|null $email
 */
class WorkerAddDTO
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string|null
     */
    public $email;

    /**
     * WorkerAddDTO constructor.
     * @param string $name
     * @param string|null $email
     */
    public function __construct(string $name, string $email = null)
    {
        $this->name = $name;
        $this->email = $email;
    }
}