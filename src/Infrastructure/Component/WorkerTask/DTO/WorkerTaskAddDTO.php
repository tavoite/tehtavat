<?php

namespace Infrastructure\Component\WorkerTask\DTO;

use Domain\Task\Entity\Task;
use Domain\Worker\Entity\Worker;

/**
 * Class WorkerTaskAddDTO
 * @package Infrastructure\Component\WorkerTask\DTO
 *
 * @property Worker $worker
 * @property Task $task
 * @property \DateTime $startDate
 * @property \DateTime $endDate
 */
class WorkerTaskAddDTO
{
    /**
     * @var Worker
     */
    public $worker;

    /**
     * @var Task
     */
    public $task;

    /**
     * @var \DateTime
     */
    public $startDate;

    /**
     * @var \DateTime
     */
    public $endDate;

    /**
     * WorkerTaskAddDTO constructor.
     * @param Worker $worker
     * @param Task $task
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function __construct(Worker $worker, Task $task, \DateTime $startDate, \DateTime $endDate)
    {
        $this->worker = $worker;
        $this->task = $task;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }
}