<?php

namespace Infrastructure\Component\WorkerTask\Service;

use Domain\Task\Repository\TaskRepositoryInterface;
use Domain\Worker\Repository\WorkerRepositoryInterface;
use Domain\WorkerTask\Service\WorkerTaskUpdateServiceInterface;
use Domain\WorkerTask\Entity\WorkerTask;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskUpdateDTO;

/**
 * Class WorkerTaskUpdateService
 * @package Infrastructure\Component\WorkerTask\Service
 */
final class WorkerTaskUpdateService implements WorkerTaskUpdateServiceInterface
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * @var WorkerRepositoryInterface
     */
    private $workerRepository;

    /**
     * WorkerTaskUpdateService constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param WorkerRepositoryInterface $workerRepository
     */
    public function __construct(TaskRepositoryInterface $taskRepository, WorkerRepositoryInterface $workerRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->workerRepository = $workerRepository;
    }

    /**
     * @inheritdoc
     */
    public function update(WorkerTask $workerTask, WorkerTaskUpdateDTO $workerTaskUpdateDTO): WorkerTask
    {
        if (!empty($workerTaskUpdateDTO->workerId)) {
            $worker = $this->workerRepository->getOne($workerTaskUpdateDTO->workerId);

            if ($worker) {
                $workerTask->changeWorker($worker);
            }
        }

        if (!empty($workerTaskUpdateDTO->taskId)) {
            $task = $this->taskRepository->getOne($workerTaskUpdateDTO->taskId);

            if ($task) {
                $workerTask->changeTask($task);
            }
        }

        if (!empty($workerTaskUpdateDTO->startDate)) {
            $workerTask->changeStartDate($workerTaskUpdateDTO->startDate);
        }

        if (!empty($workerTaskUpdateDTO->endDate)) {
            $workerTask->changeEndDate($workerTaskUpdateDTO->endDate);
        }

        return $workerTask;
    }
}