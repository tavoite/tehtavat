<?php

namespace Domain;

/**
 * Class AbstractCollection
 * @package Domain
 */
abstract class AbstractCollection implements \Countable, \Iterator
{
    /**
     * @var array
     */
    protected $items;

    /**
     * @var int
     */
    protected $position;

    /**
     * AbstractCollection constructor.
     * @param array $items
     */
    public function __construct(array $items = [])
    {
        $this->items = $items;
        $this->position = 0;
    }

    /**
     * @inheritdoc
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * @inheritdoc
     */
    public function rewind(): void
    {
        $this->position = 0;
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return $this->items[$this->position];
    }

    /**
     * @inheritdoc
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * @inheritdoc
     */
    public function next(): void
    {
        ++$this->position;
    }

    /**
     * @inheritdoc
     */
    public function valid(): bool
    {
        return array_key_exists($this->position, $this->items);
    }

    /**
     * @return array
     */
    abstract public function getItems(): array;
}