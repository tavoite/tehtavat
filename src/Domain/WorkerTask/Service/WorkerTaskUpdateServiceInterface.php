<?php

namespace Domain\WorkerTask\Service;

use Domain\WorkerTask\Entity\WorkerTask;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskUpdateDTO;

/**
 * Interface WorkerTaskUpdateServiceInterface
 * @package Domain\WorkerTask\Service
 */
interface WorkerTaskUpdateServiceInterface
{
    public function update(WorkerTask $workerTask, WorkerTaskUpdateDTO $workerTaskUpdateDTO): WorkerTask;
}