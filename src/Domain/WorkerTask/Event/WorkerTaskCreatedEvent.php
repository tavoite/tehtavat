<?php

namespace Domain\WorkerTask\Event;

use Domain\WorkerTask\Entity\WorkerTask;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class WorkerTaskCreatedEvent
 * @package Domain\WorkerTask\Event
 */
final class WorkerTaskCreatedEvent extends Event
{
    public const NAME = 'worker_task_created.event';

    /**
     * @var WorkerTask
     */
    private $workerTask;

    /**
     * WorkerTaskCreatedEvent constructor.
     * @param WorkerTask $workerTask
     */
    public function __construct(WorkerTask $workerTask)
    {
        $this->workerTask = $workerTask;
    }

    /**
     * @return WorkerTask
     */
    public function getWorkerTask(): WorkerTask
    {
        return $this->workerTask;
    }
}