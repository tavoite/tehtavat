<?php

namespace Domain\WorkerTask\Exception;

/**
 * Class WorkerTaskUpdateException
 * @package Domain\WorkerTask\Exception
 */
final class WorkerTaskUpdateException extends \Exception
{

}