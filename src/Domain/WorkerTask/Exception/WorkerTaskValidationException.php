<?php

namespace Domain\WorkerTask\Exception;

/**
 * Class WorkerTaskValidationException
 * @package Domain\WorkerTask\Exception
 */
final class WorkerTaskValidationException extends \Exception
{

}