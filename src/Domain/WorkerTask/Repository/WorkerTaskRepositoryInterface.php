<?php

namespace Domain\WorkerTask\Repository;

use Domain\Task\Entity\Task;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use Domain\WorkerTask\Exception\WorkerTaskAddException;
use Domain\WorkerTask\Exception\WorkerTaskUpdateException;
use Domain\WorkerTask\Exception\WorkerTaskValidationException;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskAddDTO;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskUpdateDTO;

/**
 * Interface WorkerTaskRepositoryInterface
 * @package Domain\WorkerTask\Repository
 */
interface WorkerTaskRepositoryInterface
{
    /**
     * @param WorkerTaskAddDTO $workerTaskAddDTO
     * @throws WorkerTaskAddException
     * @throws WorkerTaskValidationException
     * @return WorkerTask
     */
    public function add(WorkerTaskAddDTO $workerTaskAddDTO): WorkerTask;

    /**
     * @param int $id
     * @param WorkerTaskUpdateDTO $workerTaskUpdateDTO
     * @throws WorkerTaskValidationException
     * @throws WorkerTaskUpdateException
     * @return WorkerTask
     */
    public function update(int $id, WorkerTaskUpdateDTO $workerTaskUpdateDTO): WorkerTask;

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void;

    /**
     * @param int $id
     * @return WorkerTask|null
     */
    public function getOne(int $id): ?WorkerTask;

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return WorkerTaskCollection
     */
    public function getAllBetweenDates(\DateTime $startDate, \DateTime $endDate): WorkerTaskCollection;

    /**
     * @param int $quantity
     * @param bool $reverseOrder
     * @return WorkerTaskCollection
     */
    public function getLastFew(int $quantity, bool $reverseOrder = false): WorkerTaskCollection;

    /**
     * @param Task $task
     * @param int $quantity
     * @param bool $reverseOrder
     * @return WorkerTaskCollection
     */
    public function getLastFewForTask(Task $task, int $quantity, bool $reverseOrder = false): WorkerTaskCollection;

    /**
     * @param Task $task
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return bool
     */
    public function hasOneByTaskAndDates(Task $task, \DateTime $startDate, \DateTime $endDate): bool;
}