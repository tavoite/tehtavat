<?php

namespace Domain\WorkerTask\Validator\Constraints;

use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use Domain\WorkerTask\Entity\WorkerTask;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueTaskAndDatesValidator
 * @package Domain\WorkerTask\Validator\Constraints
 */
class UniqueTaskAndDatesValidator extends ConstraintValidator
{
    /**
     * @var WorkerTaskRepositoryInterface
     */
    private $workerTaskRepository;

    /**
     * UniqueTaskAndDatesValidator constructor.
     * @param WorkerTaskRepositoryInterface $workerTaskRepository
     */
    public function __construct(WorkerTaskRepositoryInterface $workerTaskRepository)
    {
        $this->workerTaskRepository = $workerTaskRepository;
    }

    /**
     * @inheritdoc
     */
    public function validate($protocol, Constraint $constraint)
    {
        /** @var WorkerTask $protocol */
        /** @var UniqueTaskAndDates $constraint */
        if ($this->workerTaskRepository->hasOneByTaskAndDates(
            $protocol->task(),
            $protocol->startDate(),
            $protocol->endDate())
        ) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }

    }
}