<?php

namespace Domain\Worker\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\Worker\Entity\WorkerCollection;
use Domain\WorkerTask\Entity\WorkerTaskCollection;

/**
 * Class AbstractWorkersPoolStrategy
 * @package Domain\Worker\Service
 */
abstract class AbstractWorkersPoolStrategy
{
    /**
     * @param WorkerCollection $workerCollection
     * @param WorkerTaskCollection $workerTaskCollection
     * @return ArrayCollection
     */
    abstract public function createWorkersPool(
        WorkerCollection $workerCollection,
        WorkerTaskCollection $workerTaskCollection
    ): ArrayCollection;
}