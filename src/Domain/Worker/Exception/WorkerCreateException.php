<?php

namespace Domain\Worker\Exception;

/**
 * Class WorkerCreateException
 * @package Domain\Worker\Exception
 */
final class WorkerCreateException extends \Exception
{

}