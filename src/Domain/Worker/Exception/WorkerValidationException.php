<?php

namespace Domain\Worker\Exception;

/**
 * Class WorkerValidationException
 * @package Domain\Worker\Exception
 */
final class WorkerValidationException extends \Exception
{

}