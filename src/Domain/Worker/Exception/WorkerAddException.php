<?php

namespace Domain\Worker\Exception;

/**
 * Class WorkerAddException
 * @package Domain\Worker\Exception
 */
final class WorkerAddException extends \Exception
{

}