<?php

namespace Domain\Worker\Exception;

/**
 * Class WorkerUpdateException
 * @package Domain\Worker\Exception
 */
final class WorkerUpdateException extends \Exception
{

}