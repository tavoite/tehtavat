<?php

namespace Domain\Worker\Exception;

/**
 * Class NoWorkerFoundException
 * @package Domain\Worker\Exception
 */
final class NoWorkerFoundException extends \Exception
{

}