<?php

namespace Domain\Worker\Entity;

use Domain\WorkerTask\Entity\WorkerTask;

/**
 * Class Worker
 * @package Domain\Worker\Entity
 */
class Worker
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string | null
     */
    private $email;

    /**
     * @var WorkerTask[]
     */
    private $tasks;

    /**
     * Worker constructor.
     * @param string $name
     * @param string|null $email
     */
    public function __construct(string $name, string $email = null)
    {
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @param string $newName
     */
    public function rename(string $newName): void
    {
        $this->name = $newName;
    }

    /**
     * @return string | null
     */
    public function email(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function changeEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name();
    }
}