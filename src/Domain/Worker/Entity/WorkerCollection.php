<?php

namespace Domain\Worker\Entity;

use Domain\AbstractCollection;

/**
 * Class WorkerCollection
 * @package Domain\Worker\Entity
 */
final class WorkerCollection extends AbstractCollection
{
    /**
     * @return Worker[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}