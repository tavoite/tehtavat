<?php

namespace Domain\Worker\Factory;

use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;

/**
 * Interface WorkerFactoryInterface
 * @package Domain\Worker\Factory
 */
interface WorkerFactoryInterface
{
    /**
     * @param WorkerAddDTO $workerDTO
     * @return Worker
     */
    public function create(WorkerAddDTO $workerDTO): Worker;

    /**
     * @param Worker[] $data
     * @return WorkerCollection
     */
    public function createCollection(array $data): WorkerCollection;
}