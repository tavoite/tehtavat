<?php

namespace Domain\Task\Entity;

use Domain\AbstractCollection;

/**
 * Class TaskCollection
 * @package Domain\Task\Entity
 */
class TaskCollection extends AbstractCollection
{
    /**
     * @return Task[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}