<?php

namespace Domain\Task\Exception;

/**
 * Class TaskRepositoryAddException
 * @package Domain\Task\Exception
 */
final class TaskAddException extends \Exception
{

}