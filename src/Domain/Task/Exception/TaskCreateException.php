<?php

namespace Domain\Task\Exception;

/**
 * Class TaskFactoryCreateException
 * @package Domain\Task\Exception
 */
final class TaskCreateException extends \Exception
{

}