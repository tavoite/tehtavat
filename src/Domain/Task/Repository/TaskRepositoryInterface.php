<?php

namespace Domain\Task\Repository;

use Domain\Task\Entity\Task;
use Domain\Task\Exception\TaskAddException;
use Domain\Task\Entity\TaskCollection;
use Infrastructure\Component\Task\DTO\TaskAddDTO;

/**
 * Interface TaskRepositoryInterface
 * @package Domain\Task\Repository
 */
interface TaskRepositoryInterface
{
    /**
     * @param TaskAddDTO $taskAddDTO
     * @throws TaskAddException
     * @return Task
     */
    public function add(TaskAddDTO $taskAddDTO): Task;

    /**
     * @return TaskCollection
     */
    public function getAll(): TaskCollection;

    /**
     * @param int $id
     * @return Task|null
     */
    public function getOne(int $id): ?Task;

    /**
     * @param string $name
     * @return Task|null
     */
    public function getOneByName(string $name): ?Task;
}