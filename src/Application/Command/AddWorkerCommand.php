<?php

namespace Application\Command;

use Domain\Worker\Exception\WorkerAddException;
use Domain\Worker\Exception\WorkerValidationException;
use Domain\Worker\Repository\WorkerRepositoryInterface;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AddWorkerCommand
 * @package Application\Command
 */
final class AddWorkerCommand extends Command
{
    private const ARGUMENT_WORKER_NAME = 'name';
    private const ARGUMENT_WORKER_EMAIL = 'email';

    /**
     * @var WorkerRepositoryInterface
     */
    private $workerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddWorkerCommand constructor.
     * @param WorkerRepositoryInterface $workerRepository
     * @param LoggerInterface $logger
     */
    public function __construct(WorkerRepositoryInterface $workerRepository, LoggerInterface $logger)
    {
        $this->workerRepository = $workerRepository;
        $this->logger = $logger;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:add-worker')
            ->setDescription('Adds new worker')
            ->addArgument(AddWorkerCommand::ARGUMENT_WORKER_NAME, InputArgument::REQUIRED, 'Worker name')
            ->addArgument(AddWorkerCommand::ARGUMENT_WORKER_EMAIL, InputArgument::OPTIONAL, 'Worker email');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $workerAddDTO = new WorkerAddDTO(
                $input->getArgument(AddWorkerCommand::ARGUMENT_WORKER_NAME),
                $input->getArgument(AddWorkerCommand::ARGUMENT_WORKER_EMAIL)
            );

            $this->workerRepository->add($workerAddDTO);

            $output->writeln('<info>Worker created</info>');
        } catch (WorkerValidationException | WorkerAddException $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}
