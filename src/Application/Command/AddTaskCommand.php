<?php

namespace Application\Command;

use Domain\Task\Exception\TaskAddException;
use Domain\Task\Repository\TaskRepositoryInterface;
use Infrastructure\Component\Task\DTO\TaskAddDTO;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AddTaskCommand
 * @package Application\Command
 */
final class AddTaskCommand extends Command
{
    private const ARGUMENT_TASK_NAME = 'name';
    private const ARGUMENT_TASK_TITLE = 'title';

    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AddTaskCommand constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param LoggerInterface $logger
     */
    public function __construct(TaskRepositoryInterface $taskRepository, LoggerInterface $logger)
    {
        $this->taskRepository = $taskRepository;
        $this->logger = $logger;

        parent::__construct(null);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:add-task')
            ->setDescription('Adds new task')
            ->addArgument(AddTaskCommand::ARGUMENT_TASK_NAME, InputArgument::REQUIRED, 'Task name')
            ->addArgument(AddTaskCommand::ARGUMENT_TASK_TITLE, InputArgument::REQUIRED, 'Task title');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $taskAddDTO = new TaskAddDTO(
                $input->getArgument(AddTaskCommand::ARGUMENT_TASK_NAME),
                $input->getArgument(AddTaskCommand::ARGUMENT_TASK_TITLE)
            );

            $this->taskRepository->add($taskAddDTO);

            $output->writeln('<info>Task created</info>');
        } catch (TaskAddException $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}