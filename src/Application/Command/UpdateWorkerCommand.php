<?php

namespace Application\Command;

use Domain\Worker\Repository\WorkerRepositoryInterface;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateWorkerCommand
 * @package Application\Command
 */
final class UpdateWorkerCommand extends Command
{
    private const ARGUMENT_ID = 'id';
    private const OPTION_NAME = 'name';
    private const OPTION_EMAIL = 'email';

    /**
     * @var WorkerRepositoryInterface
     */
    private $workerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UpdateWorkerCommand constructor.
     * @param WorkerRepositoryInterface $workerRepository
     * @param LoggerInterface $logger
     */
    public function __construct(WorkerRepositoryInterface $workerRepository, LoggerInterface $logger)
    {
        $this->workerRepository = $workerRepository;
        $this->logger = $logger;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:update-worker')
            ->setDescription('Updates existing worker')
            ->addArgument(UpdateWorkerCommand::ARGUMENT_ID, InputArgument::REQUIRED, 'Worker id')
            ->addOption(UpdateWorkerCommand::OPTION_NAME, null, InputOption::VALUE_REQUIRED)
            ->addOption(UpdateWorkerCommand::OPTION_EMAIL, null, InputOption::VALUE_REQUIRED);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $workerId = $input->getArgument(UpdateWorkerCommand::ARGUMENT_ID);
            $workerUpdateDTO = new WorkerUpdateDTO(
                $input->getOption(UpdateWorkerCommand::OPTION_NAME),
                $input->getOption(UpdateWorkerCommand::OPTION_EMAIL)
            );

            $this->workerRepository->update($workerId, $workerUpdateDTO);

            $output->writeln('<info>Worker entity updated</info>');
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}