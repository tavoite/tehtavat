<?php

namespace Application\Command;

use Domain\Task\Repository\TaskRepositoryInterface;
use Domain\Worker\Exception\NoWorkersFoundException;
use Domain\Worker\Service\AbstractRandomWorkerProviderService;
use Domain\WorkerTask\Exception\WorkerTaskAddException;
use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use Infrastructure\Component\Worker\Service\OmitLastTwoWorkersStrategy;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskAddDTO;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AssignRandomWorkerToTaskCommand
 * @package Application\Command
 */
final class AssignRandomWorkerToTaskCommand extends Command
{
    private const ARGUMENT_TASK_NAME = 'name';
    private const ARGUMENT_DATE = 'date';

    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * @var WorkerTaskRepositoryInterface
     */
    private $workerTaskRepository;

    /**
     * @var AbstractRandomWorkerProviderService
     */
    private $randomWorkerProviderService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AssignRandomWorkerToTaskCommand constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param WorkerTaskRepositoryInterface $workerTaskRepository
     * @param AbstractRandomWorkerProviderService $randomWorkerProviderService
     * @param LoggerInterface $logger
     */
    public function __construct(
        TaskRepositoryInterface $taskRepository,
        WorkerTaskRepositoryInterface $workerTaskRepository,
        AbstractRandomWorkerProviderService $randomWorkerProviderService,
        LoggerInterface $logger)
    {
        $this->taskRepository = $taskRepository;
        $this->workerTaskRepository = $workerTaskRepository;
        $this->randomWorkerProviderService = $randomWorkerProviderService;
        $this->logger = $logger;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:assign-random-worker-to-task')
            ->setDescription('Assigns random worker to specific task')
            ->addArgument(AssignRandomWorkerToTaskCommand::ARGUMENT_TASK_NAME, InputArgument::REQUIRED, 'Task name')
            ->addArgument(AssignRandomWorkerToTaskCommand::ARGUMENT_DATE, InputArgument::OPTIONAL, 'Date of assignment');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $taskName = $input->getArgument(AssignRandomWorkerToTaskCommand::ARGUMENT_TASK_NAME);
        $dateString = $input->getArgument(AssignRandomWorkerToTaskCommand::ARGUMENT_DATE) ?? '+1 day';

        $task = $this->taskRepository->getOneByName($taskName);

        if (!is_null($task)) {
            try {
                $worker = $this->randomWorkerProviderService->getRandomWorkerForTask($task, new OmitLastTwoWorkersStrategy());
                $workerTaskAddDTO = new WorkerTaskAddDTO(
                    $worker,
                    $task,
                    (new \DateTime($dateString))->setTime(0, 0, 0),
                    (new \DateTime($dateString))->setTime(23, 59, 59)
                );

                $this->workerTaskRepository->add($workerTaskAddDTO);

                $output->writeln('<info>Assign complete. Worker: ' . $worker . '</info>');
            } catch (NoWorkersFoundException | WorkerTaskAddException $exception) {
                $this->logger->error($exception->getMessage());
            }
        } else {
            $this->logger->error('Task does not exist.');
        }
    }
}