<?php

namespace Application\Command;

use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DeleteWorkerTaskCommand
 * @package Application\Command
 */
final class DeleteWorkerTaskCommand extends Command
{
    private const ARGUMENT_ID = 'id';

    /**
     * @var WorkerTaskRepositoryInterface
     */
    private $workerTaskRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * DeleteWorkerTaskCommand constructor.
     * @param WorkerTaskRepositoryInterface $workerTaskRepository
     */
    public function __construct(WorkerTaskRepositoryInterface $workerTaskRepository, LoggerInterface $logger)
    {
        $this->workerTaskRepository = $workerTaskRepository;
        $this->logger = $logger;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('app:delete-worker-task')
            ->setDescription('Deletes existing worker task entity')
            ->addArgument(DeleteWorkerTaskCommand::ARGUMENT_ID, InputArgument::REQUIRED, 'Entity id');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->workerTaskRepository->delete($input->getArgument(DeleteWorkerTaskCommand::ARGUMENT_ID));

            $output->writeln('<info>Worker task entity deleted</info>');
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
    }
}