<?php

namespace Application\Service;

use Domain\Worker\Repository\WorkerRepositoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * Class WorkerTaskAssignConfirmationService
 * @package Application\Service
 */
final class WorkerTaskAssignConfirmationService implements WorkerTaskAssignConfirmationServiceInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var WorkerRepositoryInterface
     */
    private $workerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $mailerUser;

    /**
     * @var string
     */
    private $output;

    /**
     * WorkerTaskAssignConfirmationService constructor.
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $templating
     * @param WorkerRepositoryInterface $workerRepository
     * @param LoggerInterface $logger
     * @param string $mailerUser
     */
    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $templating,
        WorkerRepositoryInterface $workerRepository,
        LoggerInterface $logger,
        string $mailerUser
    )
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->workerRepository = $workerRepository;
        $this->logger = $logger;
        $this->mailerUser = $mailerUser;
    }

    /**
     * @inheritdoc
     */
    public function send(array $data): bool
    {
        $message = (new \Swift_Message($data['task']))
            ->setFrom($this->mailerUser)
            ->setTo($this->getEmails())
            ->setBody(
                $this->templating->render('@App/emails/assign-confirmation.html.twig', [
                    'data' => $data
                ]),
                'text/html'
            );

        try {
            $this->output = $this->mailer->send($message);
        } catch (\Exception $exception) {
            $this->logger->error($exception);

            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getOutput(): string
    {
        return $this->output ?? "";
    }

    /**
     * @return array
     */
    private function getEmails(): array
    {
        $emails = [];

        foreach ($this->workerRepository->getAll()->getItems() as $worker) {
            if ($worker->hasEmail()) {
                $emails[] = $worker->email();
            }
        }

        return $emails;
    }
}