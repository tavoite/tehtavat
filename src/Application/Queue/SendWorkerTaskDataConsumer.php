<?php

namespace Application\Queue;

use Application\Service\WorkerTaskAssignConfirmationService;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class SendWorkerTaskDataConsumer
 * @package Application\Queue
 */
final class SendWorkerTaskDataConsumer implements ConsumerInterface
{
    /**
     * @var WorkerTaskAssignConfirmationService
     */
    private $workerTaskAssignConfirmationService;

    /**
     * SendWorkerTaskDataConsumer constructor.
     * @param WorkerTaskAssignConfirmationService $workerTaskAssignConfirmationService
     */
    public function __construct(WorkerTaskAssignConfirmationService $workerTaskAssignConfirmationService)
    {
        $this->workerTaskAssignConfirmationService = $workerTaskAssignConfirmationService;
    }

    /**
     * @inheritdoc
     */
    public function execute(AMQPMessage $msg)
    {
        try {
            $this->workerTaskAssignConfirmationService->send(json_decode($msg->getBody(), true));
            $output = $this->workerTaskAssignConfirmationService->getOutput();

            echo 'Mail send to ' . $output . ' recipients' . PHP_EOL;
        } catch (\Exception $exception) {
            return false;
        }
    }
}