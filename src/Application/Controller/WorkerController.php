<?php

namespace Application\Controller;

use Domain\Worker\Repository\WorkerRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WorkerController
 * @package Application\Controller
 *
 * @Route(service="app.controller.worker")
 */
final class WorkerController
{
    /**
     * @var WorkerRepositoryInterface
     */
    private $workerController;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * TaskController constructor.
     * @param WorkerRepositoryInterface $workerController
     * @param \Twig_Environment $twig
     */
    public function __construct(WorkerRepositoryInterface $workerController, \Twig_Environment $twig)
    {
        $this->workerController = $workerController;
        $this->twig = $twig;
    }

    /**
     * @Route("/worker", name="worker_index")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $workerCollection = $this->workerController->getAll();

        return new Response(
            $this->twig->render('@App/worker/index.html.twig', [
                'workerCollection' => $workerCollection
            ])
        );
    }
}