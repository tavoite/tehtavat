<?php

namespace Application\Controller;

use Domain\Task\Repository\TaskRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TaskController
 * @package Application\Controller
 *
 * @Route(service="app.controller.task")
 */
final class TaskController
{
    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * TaskController constructor.
     * @param TaskRepositoryInterface $taskRepository
     * @param \Twig_Environment $twig
     */
    public function __construct(TaskRepositoryInterface $taskRepository, \Twig_Environment $twig)
    {
        $this->taskRepository = $taskRepository;
        $this->twig = $twig;
    }

    /**
     * @Route("/task", name="task_index")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $taskCollection = $this->taskRepository->getAll();

        return new Response(
            $this->twig->render('@App/task/index.html.twig', [
                'taskCollection' => $taskCollection
            ])
        );
    }
}