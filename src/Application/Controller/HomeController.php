<?php

namespace Application\Controller;

use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomeController
 * @package Application\Controller
 *
 * @Route(service="app.controller.home")
 */
final class HomeController
{
    private const WORKER_TASK_LIMIT = 10;

    /**
     * @var WorkerTaskRepositoryInterface
     */
    private $workerTaskRepository;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * HomeController constructor.
     * @param WorkerTaskRepositoryInterface $workerTaskRepository
     * @param \Twig_Environment $twig
     */
    public function __construct(
        WorkerTaskRepositoryInterface $workerTaskRepository,
        \Twig_Environment $twig
    )
    {
        $this->workerTaskRepository = $workerTaskRepository;
        $this->twig = $twig;
    }

    /**
     * @Route("/", name="homepage")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $workerTaskCollection = $this->workerTaskRepository->getLastFew(HomeController::WORKER_TASK_LIMIT, true);

        return new Response(
            $this->twig->render('@App/home/index.html.twig', [
                'workerTaskCollection' => $workerTaskCollection
            ])
        );
    }
}