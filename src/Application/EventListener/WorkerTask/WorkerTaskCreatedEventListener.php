<?php

namespace Application\EventListener\WorkerTask;

use Domain\WorkerTask\Event\WorkerTaskCreatedEvent;
use OldSound\RabbitMqBundle\RabbitMq\Producer;

/**
 * Class WorkerTaskCreatedEventListener
 * @package Application\EventListener\WorkerTask
 */
final class WorkerTaskCreatedEventListener
{
    /**
     * @var Producer
     */
    private $producer;

    /**
     * WorkerTaskCreatedEventListener constructor.
     * @param Producer $producer
     */
    public function __construct(Producer $producer)
    {
        $this->producer = $producer;
    }

    /**
     * @param WorkerTaskCreatedEvent $event
     */
    public function onCreate(WorkerTaskCreatedEvent $event): void
    {
        $workerTask = $event->getWorkerTask();

        $this->producer->publish(json_encode([
            'worker' => $workerTask->worker()->name(),
            'task' => $workerTask->task()->title(),
            'startDate' => $workerTask->startDate()->format('Y-m-d H:i:s'),
            'endDate' => $workerTask->endDate()->format('Y-m-d H:i:s')
        ]));
    }
}