<?php

namespace Infrastructure\Component\WorkerTask\Service;

use Domain\Task\Entity\Task;
use Domain\Task\Repository\TaskRepositoryInterface;
use Domain\Worker\Entity\Worker;
use Domain\Worker\Repository\WorkerRepositoryInterface;
use Domain\WorkerTask\Entity\WorkerTask;
use Domain\WorkerTask\Service\WorkerTaskUpdateServiceInterface;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;
use Infrastructure\Component\WorkerTask\DTO\WorkerTaskUpdateDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkerTaskUpdateServiceTest
 * @package Infrastructure\Component\WorkerTask\Service
 */
class WorkerTaskUpdateServiceTest extends TestCase
{
    /**
     * @var Worker
     */
    protected $worker1;

    /**
     * @var Worker
     */
    protected $worker2;

    /**
     * @var Task
     */
    protected $task1;

    /**
     * @var Task
     */
    protected $task2;

    /**
     * @var TaskRepositoryInterface
     */
    protected $taskRepository;

    /**
     * @var WorkerRepositoryInterface
     */
    protected $workerRepository;

    public function setUp()
    {
        $this->worker1 = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->worker1->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $this->worker2 = $this->getMockBuilder(Worker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->worker2->expects($this->any())
            ->method('id')
            ->willReturn(2);

        $this->task1 = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->task1->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $this->task2 = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->task2->expects($this->any())
            ->method('id')
            ->willReturn(2);

        $this->taskRepository = $this->getMockBuilder(TaskRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->taskRepository->expects($this->any())
            ->method('getOne')
            ->willReturn($this->task2);

        $this->workerRepository = $this->getMockBuilder(WorkerRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->workerRepository->expects($this->any())
            ->method('getOne')
            ->willReturn($this->worker2);
    }

    public function testUpdateMethod()
    {
        $startDate = new \DateTime();
        $endDate = new \DateTime();
        $service = new WorkerTaskUpdateService($this->taskRepository, $this->workerRepository);

        $workerTask = new WorkerTask($this->worker1, $this->task1, $startDate, $endDate);

        $workerTaskUpdateDTO = $this
            ->getMockBuilder(WorkerTaskUpdateDTO::class)
            ->setConstructorArgs([$this->worker2->id(), $this->task2->id()])
            ->getMock();

        $updatedWorkerTask = $service->update($workerTask, $workerTaskUpdateDTO);

        $this->assertEquals($this->worker2, $updatedWorkerTask->worker());
        $this->assertEquals($this->task2, $updatedWorkerTask->task());
    }
}