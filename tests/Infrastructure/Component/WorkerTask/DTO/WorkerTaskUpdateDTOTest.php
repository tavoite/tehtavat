<?php

namespace Infrastructure\Component\WorkerTask\DTO;

use PHPUnit\Framework\TestCase;

/**
 * Class WorkerTaskUpdateDTOTest
 * @package Infrastructure\Component\WorkerTask\DTO
 */
class WorkerTaskUpdateDTOTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param int|null $workerId
     * @param int|null $taskId
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function testCreate(
        int $workerId = null,
        int $taskId = null,
        \DateTime $startDate = null,
        \DateTime $endDate = null)
    {
        $workerTaskUpdateDTO = new WorkerTaskUpdateDTO($workerId, $taskId, $startDate, $endDate);

        $this->assertEquals($workerId, $workerTaskUpdateDTO->workerId);
        $this->assertEquals($taskId, $workerTaskUpdateDTO->taskId);
        $this->assertEquals($startDate, $workerTaskUpdateDTO->startDate);
        $this->assertEquals($endDate, $workerTaskUpdateDTO->endDate);
    }

    /**
     * @return array
     */
    public function dataProvider(): array
    {
        $workerId = 1;
        $taskId = 1;
        $startDate = new \DateTime();
        $endDate = new \DateTime();

        return [
            [$workerId, $taskId, $startDate, $endDate],
            [null, null, null, null],
        ];
    }
}