<?php

namespace Infrastructure\Component\Worker\Factory;

use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Domain\Worker\Exception\WorkerCreateException;
use Domain\Worker\Factory\WorkerFactoryInterface;
use Infrastructure\Component\Worker\DTO\WorkerAddDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkerFactoryTest
 * @package Infrastructure\Component\Worker\Factory
 */
class WorkerFactoryTest extends TestCase
{
    /**
     * @var WorkerFactoryInterface $factory
     */
    protected $factory;

    public function setUp()
    {
        $this->factory = new WorkerFactory();
    }

    public function testCreateMethodSuccess()
    {
        $workerAddDTO = new WorkerAddDTO('test', 'test@test.pl');

        $worker = $this->factory->create($workerAddDTO);

        $this->assertInstanceOf(WorkerFactoryInterface::class, $this->factory);
        $this->assertInstanceOf(Worker::class, $worker);
        $this->assertEquals('test', $worker->name());
        $this->assertEquals('test@test.pl', $worker->email());
    }

    public function testCreateMethodWithNullableDTO()
    {
        $this->expectException(WorkerCreateException::class);

        $workerAddDTO = $this->getMockBuilder(WorkerAddDTO::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $this->factory->create($workerAddDTO);
    }

    public function testCreateCollectionMethodSuccess()
    {
        $worker1 = new Worker('test1', 'test1@test.pl');
        $worker2 = new Worker('test2', 'test2@test.pl');

        $collection = $this->factory->createCollection([$worker1, $worker2]);

        $this->assertInstanceOf(WorkerCollection::class, $collection);
        $this->assertEquals(2, $collection->count());
        $this->assertSame($worker1, $collection->current());

        $collection->next();

        $this->assertSame($worker2, $collection->current());
    }
}