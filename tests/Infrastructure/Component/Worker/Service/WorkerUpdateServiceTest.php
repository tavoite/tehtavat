<?php

namespace Infrastructure\Component\Worker\Service;

use Domain\Worker\Entity\Worker;
use Infrastructure\Component\Worker\DTO\WorkerUpdateDTO;
use PHPUnit\Framework\TestCase;

/**
 * Class WorkerUpdateServiceTest
 * @package Infrastructure\Component\Worker\Service
 */
class WorkerUpdateServiceTest extends TestCase
{
    /**
     * @var WorkerUpdateService
     */
    protected $service;

    public function setUp()
    {
        $this->service = new WorkerUpdateService();
    }

    public function testUpdateMethod()
    {
        $worker = new Worker('Test worker', 'worker@test.pl');

        $workerUpdateDTO = $this
            ->getMockBuilder(WorkerUpdateDTO::class)
            ->setConstructorArgs(['Updated name', 'Updated email'])
            ->getMock();

        $updatedWorker = $this->service->update($worker, $workerUpdateDTO);

        $this->assertEquals('Updated name', $updatedWorker->name());
        $this->assertEquals('Updated email', $updatedWorker->email());
    }
}