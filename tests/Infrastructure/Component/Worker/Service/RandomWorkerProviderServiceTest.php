<?php

namespace Infrastructure\Component\Worker\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\Task\Entity\Task;
use Domain\Worker\Entity\Worker;
use Domain\Worker\Entity\WorkerCollection;
use Domain\Worker\Repository\WorkerRepositoryInterface;
use Domain\Worker\Service\AbstractWorkersPoolStrategy;
use Domain\WorkerTask\Entity\WorkerTaskCollection;
use Domain\WorkerTask\Repository\WorkerTaskRepositoryInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class RandomWorkerProviderServiceTest
 * @package Infrastructure\Component\Worker\Service
 */
class RandomWorkerProviderServiceTest extends TestCase
{
    public function testGettingRandomWorkerMethod()
    {
        $worker1 = $this->createMock(Worker::class);
        $worker1
            ->expects($this->any())
            ->method('id')
            ->willReturn(1);

        $workerRepository = $this->createMock(WorkerRepositoryInterface::class);
        $workerRepository
            ->expects($this->once())
            ->method('getAll')
            ->willReturn(new WorkerCollection([$worker1]));
        $workerRepository
            ->expects($this->once())
            ->method('getOne')
            ->willReturn($worker1);

        $workerTaskRepository = $this->createMock(WorkerTaskRepositoryInterface::class);
        $workerTaskRepository
            ->expects($this->once())
            ->method('getLastFewForTask')
            ->willReturn(new WorkerTaskCollection());

        $task = $this->createMock(Task::class);
        $workersPoolStrategy = $this->createMock(AbstractWorkersPoolStrategy::class);
        $workersPoolStrategy
            ->expects($this->once())
            ->method('createWorkersPool')
            ->willReturn(new ArrayCollection([$worker1->id()]));

        $randomWorkerProviderService = new RandomWorkerProviderService($workerRepository, $workerTaskRepository);
        $randomWorker = $randomWorkerProviderService->getRandomWorkerForTask($task, $workersPoolStrategy);

        $this->assertInstanceOf(Worker::class, $randomWorker);
        $this->assertEquals($randomWorker->id(), $worker1->id());
    }
}