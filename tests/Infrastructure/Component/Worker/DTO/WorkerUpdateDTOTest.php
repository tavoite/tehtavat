<?php

namespace Infrastructure\Component\Worker\DTO;

use PHPUnit\Framework\TestCase;

/**
 * Class WorkerUpdateDTOTest
 * @package Infrastructure\Component\Worker\DTO
 */
class WorkerUpdateDTOTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param string $name
     * @param string|null $email
     */
    public function testCreate(string $name = null, string $email = null)
    {
        $workerUpdateDTO = new WorkerUpdateDTO($name, $email);

        $this->assertEquals($name, $workerUpdateDTO->name);
        $this->assertEquals($email, $workerUpdateDTO->email);
    }

    /**
     * @return array
     */
    public function dataProvider(): array
    {
        return [
            'all data' => ['test name', 'test email'],
            'without email' => ['test name 2', null],
            'without name' => [null, 'test email'],
            'without all' => [null, null],
        ];
    }
}