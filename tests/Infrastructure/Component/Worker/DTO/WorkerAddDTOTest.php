<?php

namespace Infrastructure\Component\Worker\DTO;

use PHPUnit\Framework\TestCase;

/**
 * Class WorkerAddDTOTest
 * @package Infrastructure\Component\Worker\DTO
 */
class WorkerAddDTOTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @param string $name
     * @param string|null $email
     */
    public function testCreate(string $name, string $email = null)
    {
        $workerAddDTO = new WorkerAddDTO($name, $email);

        $this->assertEquals($name, $workerAddDTO->name);
        $this->assertEquals($email, $workerAddDTO->email);
    }

    /**
     * @return array
     */
    public function dataProvider(): array
    {
        return [
            'all data' => ['test name', 'test email'],
            'without email' => ['test name 2', null]
        ];
    }
}